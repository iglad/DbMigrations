﻿using DbUp.Engine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbMigrations.Engine
{
    interface IDbUpgrader
    {
        DatabaseUpgradeResult PerformUpgrade();
        DatabaseUpgradeResult MarkAsExecuted();
    }
}
