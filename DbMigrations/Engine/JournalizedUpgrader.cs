﻿using DbMigrations.Extensions;
using DbUp;
using DbUp.Engine;
using System;
using System.Collections.Generic;

namespace DbMigrations.Engine
{
    class JournalizedUpgrader : IDbUpgrader
    {
        private UpgradeEngine dbUp = null;
        Version Version { get; set; }


        public JournalizedUpgrader(string connectionString, string dbMigrationsPath, string schema, string table, IDictionary<string, string> variables)
        {
            /* get the builder instance */
            var builder = DeployChanges.To.SqlDatabase(connectionString);

            /*configure the builder*/
            if (variables.Count > 0)
            {
                builder.WithVariablesEnabled();
                foreach (var variable in variables) { 
                    builder.Configure(c => c.Variables.Add(variable.Key, variable.Value));
                }
            }

            builder.Configure(c => c.Journal = new Journal(() => c.ConnectionManager, () => c.Log, schema, table));
            builder.Configure(c => c.ScriptExecutor = new ScriptExecutor(() => c.ConnectionManager, () => c.Log, schema, () => c.VariablesEnabled, c.ScriptPreprocessors));
            builder.Configure(c => c.ScriptProviders.Add(new RecursiveFileSystemScriptProvider(dbMigrationsPath)));
            builder.Configure(c => c.ScriptPreprocessors.Add(new SqlCmdScriptPreprocessor()));

            dbUp = builder.LogToConsole().Build();
        }

        public JournalizedUpgrader(string connectionString, string dbMigrationsPath, string schema, string table)
            : this(connectionString, dbMigrationsPath, schema, table, new Dictionary<string, string>()) { }

        public JournalizedUpgrader(string connectionString, string dbMigrationsPath, IDictionary<string, string> variables)
              :this (connectionString, dbMigrationsPath, "dbo", "SchemaVersion", variables)  { }

        public JournalizedUpgrader(string connectionString, string dbMigrationsPath)
            : this(connectionString, dbMigrationsPath, "dbo", "SchemaVersion", new Dictionary<string, string>()) { }

        public DatabaseUpgradeResult PerformUpgrade()
        {
            return dbUp.PerformUpgrade();
        }

        public DatabaseUpgradeResult MarkAsExecuted()
        {
            return dbUp.MarkAsExecuted();
        }
    }
}
