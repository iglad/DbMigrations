﻿using DbMigrations.Extensions;
using DbUp;
using DbUp.Engine;
using DbUp.Engine.Output;
using DbUp.Helpers;
using DbUp.ScriptProviders;
using DbUp.Support.SqlServer;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbMigrations.Engine
{
    class ScriptRunner : IDbUpgrader
    {
        private UpgradeEngine dbUp = null;

        public ScriptRunner(string connectionString, string path, IDictionary<string, string> variables)
        {
            /* get the builder instance */
            var builder = DeployChanges.To.SqlDatabase(connectionString);

            /*configure the builder*/
            builder.Configure(c => c.Journal = new NullJournal());
            builder.Configure(c => c.ScriptExecutor = new ScriptExecutor(() => c.ConnectionManager, () => c.Log, null, () => c.VariablesEnabled, c.ScriptPreprocessors));
            builder.Configure(c => c.ScriptProviders.Add(new FileSystemScriptProvider(path)));

            if (variables.Count > 0)
            {
                builder.WithVariablesEnabled();
                builder.Configure(c => c.Variables.Concat(variables));
            }

            dbUp = builder.LogToConsole().Build();

        }

        public ScriptRunner(string connectionString, string path) : this(connectionString, path, new Dictionary<string, string>()) {}

        public DatabaseUpgradeResult PerformUpgrade()
        {
            return this.dbUp.PerformUpgrade();
        }

        public DatabaseUpgradeResult MarkAsExecuted()
        {
            return new DatabaseUpgradeResult(new List<SqlScript>(), true, null);
        }
    }
}
