﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DbUp.Support.SqlServer;
using DbUp.Engine.Transactions;
using DbUp.Engine.Output;
using System.Data;
using DbUp.Engine;
using System.Data.SqlClient;
using System.Data.Common;
using DbMigrations.Util;

namespace DbMigrations
{
    class Journal : DbUp.Engine.IJournal
    {

        private readonly string _tableName;
        private readonly string _schemaName;
        private readonly string _schemaTableName;
        private readonly Func<IConnectionManager> _connectionManager;
        private readonly Func<IUpgradeLog> _log;

        public string upScript { get; set; }
        public string downScript { get; set; } 
             
        public Journal(Func<IConnectionManager> connectionManager, Func<IUpgradeLog> logger, string schema, string table)
        {
            _schemaName = schema;
            _tableName = table;
            _schemaTableName = string.IsNullOrEmpty(schema)
                ? SqlObjectParser.QuoteSqlObjectName(table)
                : SqlObjectParser.QuoteSqlObjectName(schema) + "." + SqlObjectParser.QuoteSqlObjectName(table);
            this._connectionManager = connectionManager;
            _log = logger;

        }

        protected virtual string CreateTableSql(string schema, string table)
        {
            return string.Format(@"create table [{0}].[{1}] (
	            [{1}Id] int identity(1,1) not null constraint PK_{1} primary key,
                [Version] varchar(50) not null,
                [VersionDescription] nvarchar(255),
    	        [ScriptName] nvarchar(255) not null,
                [ScriptContent] nvarchar(max) null,
                [ScriptChecksum] varchar(50) not null,
	            [CreateDate] datetime not null constraint DF1_{1} default (getdate()),
                [CreatedBy] varchar(200) not null constraint DF2_{1} default (suser_sname())
                )", schema, table);
        }

        protected virtual string CreateTableSql()
        {
            return CreateTableSql(_schemaName, _tableName);
        }

        /// <summary>
        /// Recalls the version number of the database.
        /// </summary>
        /// <returns>All executed scripts.</returns>
        public string[] GetExecutedScripts()
        {
            _log().WriteInformation("Fetching list of already executed scripts.");
            var exists = DoesTableExist();
            if (!exists)
            {
                _log().WriteInformation(string.Format("The {0} table could not be found. The database is assumed to be at version 0.", _schemaTableName));
                return new string[0];
            }

            var scripts = new List<string>();
            _connectionManager().ExecuteCommandsWithManagedConnection(dbCommandFactory =>
            {
                using (var command = dbCommandFactory())
                {
                    command.CommandText = GetExecutedScriptsSql(_schemaTableName);
                    command.CommandType = CommandType.Text;

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                            scripts.Add((string)reader[0]);
                    }
                }
            });

            return scripts.ToArray();
        }

        /// <summary>
        /// The Sql which gets 
        /// </summary>
        protected virtual string GetExecutedScriptsSql(string table)
        {
            return string.Format("select [ScriptName] from {0} order by [ScriptName]", table);
        }

        /// <summary>
        /// Records a database upgrade for a database specified in a given connection string.
        /// </summary>
        /// <param name="script">The script.</param>
        public void StoreExecutedScript(SqlScript script)
        {
            var exists = DoesTableExist();
            if (!exists)
            {
                _log().WriteInformation(string.Format("Creating the {0} table", _schemaTableName));

                _connectionManager().ExecuteCommandsWithManagedConnection(dbCommandFactory =>
                {
                    using (var command = dbCommandFactory())
                    {
                        command.CommandText = CreateTableSql(_schemaName, _tableName);

                        command.CommandType = CommandType.Text;
                        command.ExecuteNonQuery();
                    }

                    _log().WriteInformation(string.Format("The {0} table has been created", _schemaTableName));
                });
            }

            var version = DbVersion.GetVersion(script.Name);

            _connectionManager().ExecuteCommandsWithManagedConnection(dbCommandFactory =>
            {
                using (var command = dbCommandFactory())
                {
                    command.CommandText = string.Format(@"
                        insert into {0} 
                            (Version, VersionDescription, ScriptName, ScriptContent, ScriptCheckSum) 
                        values 
                            (@version, @versionDescription, @scriptName, @commandText, @scriptChecksum)", 
                        _schemaTableName);
                    
                    var scriptNameParam = command.CreateParameter();
                    scriptNameParam.ParameterName = "scriptName";
                    scriptNameParam.Value = script.Name;
                    command.Parameters.Add(scriptNameParam);

                    var sqlCommand = command.CreateParameter();
                    sqlCommand.ParameterName = "commandText";
                    sqlCommand.Value = script.Contents;
                    command.Parameters.Add(sqlCommand);

                    var versionParam = command.CreateParameter();
                    versionParam.ParameterName = "version";
                    versionParam.Value = version.ScriptVersion.ToString(4);
                    command.Parameters.Add(versionParam);

                    var versionDescription = command.CreateParameter();
                    versionDescription.ParameterName = "versionDescription";
                    versionDescription.Value = version.VersionDescription;
                    command.Parameters.Add(versionDescription);

                    var scriptCheckSum = command.CreateParameter();
                    scriptCheckSum.ParameterName = "scriptChecksum";
                    scriptCheckSum.Value = GetStringCheckSum(script.Contents);
                    command.Parameters.Add(scriptCheckSum);

                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                }
            });
        }

        private string GetStringCheckSum(string theString)
        {
            string hash;
            using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
            {
                hash = BitConverter.ToString(
                  md5.ComputeHash(Encoding.UTF8.GetBytes(theString))
                ).Replace("-", String.Empty);
            }
            return hash;
        }

        private bool DoesTableExist()
        {
            return _connectionManager().ExecuteCommandsWithManagedConnection(dbCommandFactory =>
            {
                try
                {
                    using (var command = dbCommandFactory())
                    {
                        command.CommandText = string.Format("select count(*) from {0}", _schemaTableName);
                        command.CommandType = CommandType.Text;
                        command.ExecuteScalar();
                        return true;
                    }
                }
                catch (SqlException)
                {
                    return false;
                }
                catch (DbException)
                {
                    return false;
                }
            });
        }
    }
}
