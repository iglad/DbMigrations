﻿using DbUp.Engine;
using NLog;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DbMigrations.Extensions
{
    class PatternVariableSubstitutionPreprocessor : IScriptPreprocessor
    {
        private readonly IDictionary<string, string> variables;
        private Regex tokenRegex; // = new Regex(@"\$(?<variableName>\w+)\$");
        private static Logger logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Initializes a new instance of the <see cref="PatternVariableSubstitutionPreprocessor"/> class.
        /// </summary>
        /// <param name="variables">The variables.</param>
        public PatternVariableSubstitutionPreprocessor(IDictionary<string, string> variables, Regex patern)
        {
            this.variables = variables;
            this.tokenRegex = patern; 
        }

        public PatternVariableSubstitutionPreprocessor(IDictionary<string, string> variables) :this(variables, new Regex(@"(\$\()(\w+)(\))")) { }

        /// <summary>
        /// Substitutes variables 
        /// </summary>
        /// <param name="contents"></param>
        public string Process(string contents)
        {
            return tokenRegex.Replace(contents, match => ReplaceToken(match, variables));
        }

        private static string ReplaceToken(Match match, IDictionary<string, string> variables)
        {
            var variableName = match.Groups[2].Value;
            if (!variables.ContainsKey(variableName))
            {
                logger.Fatal("The variable [{0}] is not configured", variableName);
            }
               
            return variables[variableName];
        }
   }
}
