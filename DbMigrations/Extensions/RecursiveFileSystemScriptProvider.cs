﻿using DbUp.Engine;
using DbUp.Engine.Transactions;
using DbUp.ScriptProviders;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DbMigrations.Extensions
{
    class RecursiveFileSystemScriptProvider : IScriptProvider
    {
        private readonly string directoryPath;
        private readonly Func<string, bool> filter;

         ///<summary>
        ///</summary>
        ///<param name="directoryPath">Path to SQL upgrade scripts</param>
        public RecursiveFileSystemScriptProvider(string directoryPath)
        {
            this.directoryPath = directoryPath;
            this.filter = null;
        }

        ///<summary>
        ///</summary>
        ///<param name="directoryPath">Path to SQL upgrade scripts</param>
        ///<param name="filter">The filter.</param>
        public RecursiveFileSystemScriptProvider(string directoryPath, Func<string, bool> filter)
        {
            this.directoryPath = directoryPath;
            this.filter = filter;
        }

        /// <summary>
        /// Gets all scripts that should be executed.
        /// </summary>
        /// <param name="connectionManager"></param>
        /// <returns>IEnumerable<SqlScript></returns>
        public IEnumerable<SqlScript> GetScripts(IConnectionManager connectionManager)
        {
            return GetFiles(directoryPath).Select(SqlScript.FromFile).ToList(); ;
        }

        /// <summary>
        /// Get all files in the given folder and its subfolders
        /// </summary>
        /// <param name="path"></param>
        /// <returns>IEnumerable<string></returns>
        private IEnumerable<string> GetFiles(string path)
        {
            var files = Directory.GetFiles(path, "*.sql", SearchOption.AllDirectories).AsEnumerable();
            if (this.filter != null)
            {
                files = files.Where(filter);
            }
            
            return files;
        }

        private void StripJunk(IEnumerable<string> files)
        {
            foreach (var file in files)
            {
                StripSqlCmd(file);
            }
        }
        private void StripSqlCmd(string path)
        {
            var document = File.ReadAllText(path);
            var result = Regex.Replace(document, @"^:.*(\r\n|\r|\n)", String.Empty, RegexOptions.IgnoreCase | RegexOptions.Multiline);
            File.WriteAllText(path, result);
        }
    }
}
