﻿using DbUp.Engine;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DbMigrations.Extensions
{
    /// <summary>
    /// Takes out an SQLCmd commands such as :setvar or :onerror
    /// </summary>
    class SqlCmdScriptPreprocessor : IScriptPreprocessor
    {

        public string Process(string contents)
        {
            var result = Regex.Replace(contents, @"^:.*(\r\n|\r|\n)", String.Empty, RegexOptions.IgnoreCase | RegexOptions.Multiline);
            return result;
        }

    }
}
