﻿using System.Runtime.Remoting.Messaging;
using DbUp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using DbUp.Builder;
using System.Configuration;
using DbMigrations.Extensions;
using DbUp.Engine.Output;
using NDesk.Options;
using System.Data.SqlClient;
using DbUp.Engine;
using DbMigrations.Util;
using DbMigrations.Engine;
using System.Collections.Specialized;
using NLog;
using DbUp.Helpers;

namespace DbMigrations
{
    class Program
    {
        static void Main(string[] args)
        {
            var Log = new TraceUpgradeLog();
            var environment = "";
            var server = "";
            var database = "";
            var path = "";
            var username = "";
            var password = "";
            bool mark = false;
            var connectionString = "";
            bool runOnce = false;
            var version = new Version("0.01.00");
            bool show_help = false;
            var configFilePath = "";
            var majorVersion = 1;
            var minorVersion = 0;
            var checkDbVersionExtendedProperties = false;
                
            var optionSet = new OptionSet() {
                { "o|runOnce", "run scripts once (Journalized Upgrade)", o => runOnce = true },
                { "s|server=", "the SQL Server host", s => server = s },
                { "db|database=", "database to upgrade", d => database = d},
                { "d|directory=", "directory containing SQL Update files [required]", dir => path = dir },
                { "u|user=", "Database user name", u => username = u},
                { "p|password=", "Database password", p => password = p},
                { "cs|connectionString=", "Full connection string", cs => connectionString = cs},
                { "h|help",  "show this message and exit", v => show_help = v != null },
                {"mark", "Mark scripts as executed but take no action", m => mark = true},
                { "e|connectionStringName=", "the connection string name (e.g. omDev or coreApiTest) [required]", e => environment = "local"},
                { "c|configFile=", "a dynamic configurations file path", c => configFilePath = c },
                { "X|checkDbVersion=", "check DBVersion among DB Extended properties", X => checkDbVersionExtendedProperties = X != null},
            };

            optionSet.Parse(args);

            if (args.Length == 0 || String.IsNullOrEmpty(path))
                show_help = true;

#if !DEBUG
            if (show_help)
            {
                optionSet.WriteOptionDescriptions(System.Console.Out);
                Console.Read();
                return;
            }
#endif
#if DEBUG
            checkDbVersionExtendedProperties = true;
            runOnce = true;
            path = @"C:\Users\igladyshev\DBProjects\DBMigrations\msbuild\HA_OrderManagement\NonSSDT_Release";
            environment = "local";
            connectionString = "Data Source=localhost;Initial Catalog=HA_OrderManagement;Integrated Security=True";
            //configFilePath = @"C:\Users\igor\Documents\Visual Studio 2015\Projects\AdventureWorks\AdventureWorks\DbMigrations\variables.config";
            mark = true;
            
#endif
            /*Build connection string from arguments*/
            if (String.IsNullOrEmpty(connectionString) && !String.IsNullOrEmpty(server) && !String.IsNullOrEmpty(database))
            {
                connectionString = BuildConnectionString(server, database, username, password);
            }

            /*read configuration parameters*/
            if (!String.IsNullOrEmpty(configFilePath))
            {
                LoadCustomConfiguration(configFilePath);
            }

            if (String.IsNullOrEmpty(connectionString) && ConfigurationManager.AppSettings.AllKeys.Contains("db.host") && ConfigurationManager.AppSettings.AllKeys.Contains("db.name"))
            {
                server = ConfigurationManager.AppSettings["db.host"];
                database = ConfigurationManager.AppSettings["db.name"];

                connectionString = BuildConnectionString(server, database, username, password);
            }

            if (String.IsNullOrEmpty(connectionString) && !String.IsNullOrEmpty(environment))
            {
                var connectionStrings = ConfigurationManager.ConnectionStrings;
                connectionString = connectionStrings[environment].ConnectionString;
            }

            /*get variables collection*/
            var variables = new Dictionary<string, string>();
            var variablesSection = ConfigurationManager.GetSection("globalVariables") as NameValueCollection;
            foreach (var key in variablesSection.AllKeys)
            {
                variables.Add(key.ToString(), variablesSection.GetValues(key).FirstOrDefault().ToString());
            }

            //variablesSection = ConfigurationManager.GetSection("environmentVariables") as NameValueCollection;
            var varQuery = from a in ConfigurationManager.AppSettings.AllKeys where a.StartsWith("var") select a;

            foreach (var key in varQuery)
            {
                variables.Add(key, ConfigurationManager.AppSettings[key]);
            }

            if (variables.ContainsKey("DbMigrationsVersion"))
            {
                version = new Version(variables["DbMigrationsVersion"].ToString());
            }
            var versionTable = ConfigurationManager.AppSettings["schemaVersionTableName"] ?? "SchemaVersion";
            var versionSchema = ConfigurationManager.AppSettings["schemaVersionSchemaName"] ?? "dbo";
            var sMajorVersion = ConfigurationManager.AppSettings["majorDbVersion"] ?? "1";
            var sMinorVersion = ConfigurationManager.AppSettings["majorDbVersion"] ?? "00";

            if (checkDbVersionExtendedProperties)
            {
                sMajorVersion = SetDbExtendedProperty(connectionString, "MajorVersion", sMajorVersion);
                sMinorVersion = SetDbExtendedProperty(connectionString, "MinorVersion", sMinorVersion);
            }

            majorVersion = Int16.Parse(sMajorVersion);
            minorVersion = Int16.Parse(sMinorVersion);
            

            IDbUpgrader dbup = null;
            if (runOnce)
            {
                dbup = new JournalizedUpgrader(connectionString, path, versionSchema, versionTable, variables);
            }
            else
            {
                dbup = new ScriptRunner(connectionString, path, variables);
            }

            /*perform upgrade*/
            DatabaseUpgradeResult result = null;
            if (!mark)
            {
                result = dbup.PerformUpgrade();
            }
            else
            {
                result = dbup.MarkAsExecuted();
            }

            if (!result.Successful)
            {
                Environment.ExitCode = 1;
            }
        }

        private static string BuildConnectionString(string server, string database, string username, string password)
        {
            var conn = new SqlConnectionStringBuilder();
            conn.DataSource = server;
            conn.InitialCatalog = database;
            if (!String.IsNullOrEmpty(username))
            {
                conn.UserID = username;
                conn.Password = password;
                conn.IntegratedSecurity = false;
            }
            else
            {
                conn.IntegratedSecurity = true;
            }

            return conn.ToString();
        }


        private static void LoadCustomConfiguration(string path)
        {
            var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            config.AppSettings.File = path;
            config.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection("appSettings");
        }

        private static string SetDbExtendedProperty(string connectionString, string name, string value)
        {
            string currentValue = value;
            using (var conn = new SqlConnection(connectionString))
            using (var cmd = conn.CreateCommand())
            {
                conn.Open();
                cmd.CommandText = "SELECT value FROM sys.extended_properties WHERE class = 0 AND name = @name";
                cmd.Parameters.AddWithValue("@name", name);
                currentValue = cmd.ExecuteScalar().ToString();
                if (currentValue != value)
                {
                    conn.CreateCommand();
                    cmd.CommandText = "EXEC sys.sp_addextendedproperty @name, @value";
                    cmd.Parameters.AddWithValue("@name", name);
                    cmd.Parameters.AddWithValue("@value", value);
                    cmd.ExecuteNonQuery();
                }
            }
            return value;
        }
    }
}
