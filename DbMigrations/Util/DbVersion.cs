﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using DbUp.Engine.Output;
using System.Configuration;
using NLog;

namespace DbMigrations.Util
{
    class DbVersion 
    {
        public static string VersionPreffix { get; set; }
        public static string VersionSeparator { get; set; }
        public string VersionDescription { get; set; }
        public Version ScriptVersion { get; set; }
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public string VersionChecksum { get; set; }

        /// <summary>
        /// This static method gets a script version even the script doesn't include one
        /// For instance, if the script name were just JIRA-1234.sql, it has got generated version"
        ///     Script name: JIRA-1234.sql -> Version: V
        /// </summary>
        /// <param name="scriptName"></param>
        /// <returns></returns>
        public static DbVersion GetVersion(string scriptName, int majorVersion = 1, int minorVersion = 0)
        {
            DbVersion version = null;
            try 
            {
                version = new DbVersion(scriptName);
            }
            catch (Exception)
            {
                scriptName = DbVersion.VersionPreffix + 
                    string.Format("{0:0}", majorVersion) + "." + 
                    string.Format("{0:00}", minorVersion) + "." +
                    DateTime.Now.ToString("yyyyMMdd.HHmmssff") + DbVersion.VersionSeparator + scriptName;
                version = new DbVersion(scriptName);
            }

            return version;
        }

        public DbVersion(string scriptName)
        {
            VersionPreffix = ConfigurationManager.AppSettings["versionPrefix"] ?? "V";
            VersionSeparator = ConfigurationManager.AppSettings["versionSeparator"] ?? "__";

            string[] versionInfo = Regex.Split(scriptName, VersionSeparator);
            string versionString = null;
            try
            {
                versionString = versionInfo[0].Replace(VersionPreffix, "");
                VersionDescription = versionInfo[1].Replace("_", " ").Replace(".sql", "");
                ScriptVersion = new Version(versionString);

            }
            catch (ArgumentNullException e)
            {
                logger.Error("Error: String to be parsed is null.");
                throw (e);
            }
            catch (ArgumentOutOfRangeException e)
            {
                logger.Error("Error: Negative value in '{0}'.", versionString);
                throw (e);
            }
            catch (ArgumentException e)
            {
                logger.Error("Error: Bad number of components in '{0}'.", versionString);
                throw (e);
            }
            catch (FormatException e)
            {
                logger.Error("Error: Non-integer value in '{0}'.", versionString);
                throw (e);
            }
            catch (OverflowException e)
            {
                logger.Error("Error: Number out of range in '{0}'.", versionString);
                throw (e);
            }   
        }
    }
}
