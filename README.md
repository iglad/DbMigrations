This is an extension of the DbUp (http://dbup.github.io/) database migrations tool.
Extesnions made:
- Recursive file system script provider implemented
- Extended SchemaVersion table implemented with the followng structure:

>
    create table [{0}].[{1}] (
    [{1}Id] int identity(1,1) not null constraint PK_{1} primary key,
        [Version] varchar(50) not null,
        [VersionDescription] nvarchar(255),
        [ScriptName] nvarchar(255) not null,
        [ScriptContent] nvarchar(max) null,
        [ScriptChecksum] varchar(50) not null,
    	[CreateDate] datetime not null constraint DF1_{1} default (getdate()),
        [CreatedBy] varchar(200) not null constraint DF2_{1} default (suser_sname())
    )
>

- Version parser added to the SqlScriptExecuter
- Implemented two db upgrade flows:
    - Run once (journalized db migration)
    - Run always - executes scripts at given location
- Added support of script version simulation when the script name is not formatted for versioning

>
    JIRA-1234.sql => V1.00.20151102.08122334__JIRA-1234.sql
>

- Major and Minor database versions could be configured via config file or/and via database extended properties